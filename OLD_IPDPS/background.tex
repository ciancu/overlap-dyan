\section{Background and Motivation}
\label{sec:bm}
 
One-sided communication exploits networking hardware support
for Remote Direct Memory Access (RDMA).  Library based approaches
(uGNI/DMAPP~\cite{crayxc}, IBVerbs, 
OpenSHMEM~\cite{shmem},  MPI  RMA~\cite{mpi}) expose to programmers a communication interface for {\it Put/Get} primitives, augmented with {\it (try\_)wait} completion
operations, and with synchronizations  such as {\it barriers}, {\it fences} etc. Languages  (UPC~\cite{upc}, X10~\cite{x10}, Chapel~\cite{chapel}) add support in the type system for building a Global Address 
Space and memory consistency models. 



Our target language is  Unified Parallel C (UPC), a good representative of the family of Global Address Space languages. In UPC, memory is divided into thread private and global spaces. The global address space is further 
partitioned logically into portions that are local to a task. Thread private data can be accessed only through proper C pointer data types. All global data can be accessed through ``pointers to shared'' type extensions, while a task may alternatively use a C pointer to access its portion of it. The language implements Single Program Multiple Data (SPMD) parallelism. 

Communication in UPC is one-sided, and can be either implicit or
explicit. Implicit communication happens whenever variables of type
``pointer to shared'' appear in an expression. Optimizing implicit
communication in UPC, and all other GAS languages, is outside
programmer control and requires compiler support. The language also
exposes blocking {\it Put/Get} primitives for explicit communication,
e.g. \texttt{upc\_memput(shared void *dst, void *src, size\_t n)},
where {\tt *dst} encodes both a thread identifier and a memory
address. A blocking operation returns only when
the data has arrived and has been written to the memory of the remote
node. Both implicit and explicit blocking communication are subject to the language memory consistency model and can be optimized by compilers.  
These features are common to other GAS languages such as X10 and Chapel. 

To facilitate manual optimizations, library extensions for 
nonblocking communication have been adopted. These take the form {\tt
  \{init(); ... wait();\}}, where {\tt init} initializes a nonblocking
operation in hardware and {\tt wait} checks for its completion. Optimizations for communication overlap try to find
independent code to execute between the two operations. The nonblocking communication
is outside the scope of the language memory consistency model and it is
not directly optimized by existing compilers. The language level
nonblocking interfaces are close in spirit to the underlying low level
networking API used for implementation of the language runtime: uGNI,
DMAPP, IBVerbs, OpenSHMEM etc.



The UPC language also provides synchronization primitives (fences,
barriers, locks) to explicitly enforce data dependence and that are representative for the family of one-sided paradigms. A \texttt{upc\_fence}  quiesces all outstanding communication within a thread. Note though that nonblocking extensions are not subject to these fences. Similar constructs appear in OpenSHMEM, uGNI, MPI RMA etc.
A   \texttt{upc\_barrier} provides a global synchronization point. Similar constructs appear in MPI, DMAPP, OpenSHMEM and IBVerbs.
Lock primitives are provided by the UPC language
for building mutual exclusion regions.  To allow for a richer set of
synchronization patterns, library extensions have been introduced. One
example is the Berkeley UPC~\cite{bupc} extension which introduces semaphores to build
custom point-to-point synchronization patterns. The semaphores are
plain counting semaphores with a {\tt signal() <-> wait()} interface
and can be used to efficiently  build complex Producer-Consumer
relationships. While nonblocking communication primitives are
standardized in the UPC specification as a runtime library API, these
third party synchronization primitives may be redefined with  ad-hoc semantics
across implementations. 

Usually (P)GAS languages have multiple runtime implementations. Vendor
compilers such as IBM X10 or Cray Chapel have runtime implementations
targeting directly the system native  communication API, PAMI and DMAPP
respectively. They also provide MPI or GASNet based implementations for
portability. The UPC compiler we use runs on top of GASNet. GASNet is
a lightweight portability layer across all the low level system
communication APIs used in supercomputers. It is the case that any low
level communication API provides a much richer and flexible set of
interfaces than exposed at the language or application level. We refer
to this as a functional semantic mismatch.  The question remains how
to exploit this low level functionality directly at the application
level without  non-portable changes to the source code.




\subsection{Optimizing One-Sided Communication}


Unlike two sided MPI Send/Recv communication, one-sided transfers
decouple data transmission from inter-thread synchronization 
and allow for sophisticated communication and synchronization
operations.  RDMA Put/Get operations only involve the sender of a data communication, in
contrast to MPI two-sided communication, which involves a hand-shake between
sender and receiver.
Only the sender can verify the completion of the RDMA operation and a
further, programmatic, synchronization between sender and receiver is
needed as a means to signal this completion.


Nonblocking one-sided communication opens the possibility of latency
hiding optimizations and communication overlap.   In the optimal case,
the application never waits for the completion of any communication
operation: it executes independent code after {\tt init} that takes
longer than the duration of communication. 
Thus, it is key to delay both communication completion and synchronization until the furthest logically correct point
in the program. For the explicit
primitives most often this is  manually attained %~\cite{Bell:2006,Nishtala:2009} 
by
interposing independent code between the {\tt init-wait} pair of
calls. Previous work described compiler optimizations for overlapping
implicit communication, but to our knowledge these optimizations are
not deployed in production compilers, due to engineering
challenges. Furthermore, most are local in scope, both in terms of the
domain decomposition and code module, i.e.  encapsulated in
specialized transfer functions for each domain boundary. This is
further detailed in Section~\ref{sec:trans}. Localization and
modularity results in conservative optimizations. For example, in
complicated code bases it is hard for developers to associate
synchronization operations (e.g. barriers) exactly  with the transfers
whose consistency they are introduced to maintain. This results in
conservative design decisions and possible loss of communication
overlap. We refer to this as logical semantic mismatch.  The question
remains if this behavior is present in application and whether we can
optimize around it.

Nonblocking collective operations~\cite{Hoefler2007} is now available for MPI and is currently discussed for adoption in
the OpenSHMEM standard. To our knowledge, very little work
has been performed in developing application level optimizations for such
primitives. Furthermore, we are not aware of any automated optimization
techniques for applications containing ad-hoc synchronization
patterns. The question remains if these can be deployed in existing
applications without restructuring and if they improve performance. 

In the rest of paper, for brevity we will  use the
terms\footnote{Another taxonomy can be based on static and dynamic
  information.}  local dependence and
global or inter-thread dependence. Local  refers to any data
dependence between communication (e.g. Put), local memory accesses
(e.g. store) and inter-thread synchronization operations. In the ``local'' view we
do not distinguish between the targets of synchronization and any
synchronization operation introduces a data dependence. This is
similar to what static program analysis can accomplish.  In the
``global''  view, we take into account the program dynamic behavior
and add dependencies only between communication and synchronization
operations with the same task. 


To address the  perceived limitations of existing
approaches we develop an optimization methodology to:
\begin{itemize}
\item Provide maximal overlap with respect to local and inter-thread
  dependencies and synchronization. 
\item Perform global optimizations  across modules at same level of
  abstraction and bridge any logical semantic gap. Perform global optimizations across modules at different levels of
  abstraction and bridge functional semantic gaps.
\item Allow for easy experimentation with new primitives (e.g. nonblocking
  collectives) and transformations.
\end{itemize}





