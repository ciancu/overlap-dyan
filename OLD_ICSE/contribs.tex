

\section{Implementation of Dynamic Analysis Toolchain}
\label{sec:contrib1}


Our Toolchain comprises three components. The first pass is a
compile-time instrumentation of the code to collect data and control
dependence information about the executing program. The output of a
run is recorded in trace files, and we record a file per thread of
interest. 

A second pass merges the trace files and attempts to perform
the optimizations. As maximal overlap with respect to local thread
data dependence is an enabler for the other optimizations, it is
performed first.  For each operation of interest,  e.g {\tt init,
  wait, barrier\_notify}, this stage associates to the original
location in the source file the list of source locations across all
monitored threads where the operation can be moved to. The output of
this stage is presented to the application developers for
validation. This last step is necessary since we cannot guarantee the
soundness of the transformations across all input sets and
concurrencies using dynamic program analysis. 

Finally, the third component can automatically generate the code to implement
the transformations desired by the application developers. The input
to this stage is again a list of associations between source locations
where the operations of interest should be moved to. 







\subsection{Trace File Generation}

We use the LLVM~\cite{LLVMwebsite} compiler infrastructure to instrument the
target program. For each bitcode file we instrument the LLVM IR nodes
of interest, which are either instructions or a priori selected
function calls.  A relevant instruction is any instruction accessing
memory (e.g. load or store)  and intrinsic synchronization
operations. We can also capture synchronization functions related to the
programming model or the underlying  communication runtime, for the
cases where we decide they have generic enough semantics.  Examples
include SPMD  barriers, fence or primitives for  point-to-point
synchronization using semaphores.

At compile time, each operation of interest is surrounded by tracing
calls. This allows us to not only track state, but to split operations
into their components.  For example for explicitly blocking communication,
e.g. {\tt upc\_memput}, we need to generate the {\tt init, wait}
components. Similarly barriers are split into notify/wait pairs in the
trace file.  The tool allows to easily extend the set of tracked operations
using link time interposition.

The instrumentation emits runtime events into a trace file associated
with each thread.  
As tracing introduces significant runtime overhead, we  use optimization passes to
reduce the number of load and store instructions that are
instrumented. We use the alias analysis as well as heuristics related
to the UPC programming model. For example, automatic variables can be
ignored, or we try to ensure that  only loads
and stores on local memory addresses involved in a data transfer
(e.g. Put)
function are recorded. Note that this last goal motivated our design
decision to instrument certain functions at compile time, rather than
having any runtime call instrumented though link time interposition. 


\subsection{Trace File Analysis}

An analysis pass processes the trace files to determine the optimal
placement for the operations of interest, considering the three target
transformations.  For brevity, we give only an intuitive description
of this stage.

\subsubsection{Maximal Overlap Analysis}
Consider a snippet of the trace containing the events  $ e_i ~ e_{i+1}
~ ... ~ e_j$. 
Assume event $e_i$ is one of the communication functions (Put) and the
next event $e_{i+1}$ corresponds to its completion as presented Figure
\ref{fig:optim}. Assume that $e_j$ is the first event in the trace,
after $e_i$, which  requires that communication initiated in $e_i$ is
completed in order to maintain consistency.  For Puts, these events are illustrated in
Table~\ref{tab:TFA}: i)  a data transfer function must complete before
the destination memory is read and the source memory is written;  ii)
a data transfer function must complete before a synchronization
operation.  Note that this is only a simplified description of
  the analysis or event association. The analysis follows use-def
  chains across synchronization operations.  Some applications we tested contain redundant
  barriers and the analysis   can delay communication
  completion before the last redundant barrier.



\begin{table}[ht!]
\centering
\caption{Event association: $la$ = local address, $sa$ = shared
  address, $t$ = time stamp, $tid$ = thread, $loc$ = source location}
{\small
\begin{tabular}{|c|c|} \hline 
{\bf{$e_i$}} 										&    {\bf{$e_j$}}	 \\        								\hline
 \multirow{6}*{$PUT(sa, la_i, t_{e_i}, tid_{e_i}, loc_{e_i})$}     	&  $\emph{GET}(la_i,*,t_{e_j}, tid_{e_j}, loc_{e_j})$, \\
 												&  $\emph{STORE}(la_i, t_{e_j}, tid_{e_j}, loc_{e_j})$, \\
								     				&  $\emph{CPY/GET}(*, sa, t_{e_j}, tid_{e_j}, loc_{e_j})$, \\
												&  $\emph{LOAD}(sa, t_{e_j}, tid_{e_j}, loc_{e_j})$ \\
												&  $\emph{BARRIER}(t_{e_j}, tid_{e_j}, loc_{e_j})$ or \\
												&  $\emph{SYNCHRO}(t_{e_j}, tid_{e_j}, loc_{e_j})$	 \\ 		\hline
 \comment{\multirow{6}*{$GET(la_i, sa, t_{e_i}, tid_{e_i}, loc_{e_i})$}  	&  $\emph{PUT/CPY}(sa,*, t_{e_j}, tid_{e_j}, loc_{e_j})$, \\
												&  $\emph{STORE}(sa,t_{e_j}, tid_{e_j}, loc_{e_j})$, \\ 
												&  $\emph{LOAD}(la_i, t_{e_j}, tid_{e_j}, loc_{e_j})$, \\
												&  $\emph{PUT}(*,la_i,t_{e_j}, tid_{e_j}, loc_{e_j})$, \\ 
												&  $\emph{BARRIER}(t_{e_j}, tid_{e_j}, loc_{e_j})$ or \\
												&  $\emph{SYNCHRO}(t_{e_j}, tid_{e_j}, loc_{e_j})$  \\ 		\hline
\multirow{6}*{$CPY(sa, sa',  t_{e_i}, tid_{e_i}, loc_{e_i})$}		&  $\emph{PUT/CPY}(sa',*, t_{e_j}, tid_{e_j}, loc_{e_j})$, \\
												&  $\emph{STORE}(sa', t_{e_j}, tid_{e_j}, loc_{e_j})$, \\
												&  $\emph{CPY/GET}(*,sa, t_{e_j}, tid_{e_j}, loc_{e_j})$,  \\
												&  $\emph{LOAD}(sa, t_{e_j}, tid_{e_j}, loc_{e_j})$,\\ 
												&  $\emph{BARRIER}(t_{e_j}, tid_{e_j}, loc_{e_j})$ or \\
												&  $\emph{SYNCHRO}(t_{e_j}, tid_{e_j}, loc_{e_j})$. \\ \hline 		
}
\end{tabular}
}
\label{tab:TFA}
\end{table}



\begin{figure}[ht!]
\centering
\begin{small}
\begin{tikzpicture}
\draw (0,1) node[above] {{\bf{Before optimization}}};
\draw (0,0.5) node[above] {$e_i = PUT(...)$};
\draw (0,0) node[above] {$e_{i+1} = SYNC(e_i, t_{e_i}, tid_{e_i}, loc_{e_i})$};
\draw[dotted] (0,-0.1) -- (0,-0.5);
\draw (0,-1) node[above] {$e_j$ (see\ Table\ I)};
%%%%
\draw (4,1) node[above] {{\bf{After optimization}}};
\draw (4,0.5) node[above] {$e_i$};
\draw[dotted] (4,0.2) -- (4,-0.1);
\draw (4,-0.5) node[above] {$e_{i+1}$};
\draw (4,-1) node[above] {$e_j $};
\end{tikzpicture}
\end{small}
\caption{Code transformation for maximal overlap with respect to local
dependencies.}
\label{fig:optim}
\end{figure}







\subsubsection{Optimizing Across Barriers and Bespoke Synchronization}

Consider a barrier operation, that is replaced in the trace by the
pair of notify/wait components. Any notify can be moved higher after
the last read/write on a shared variable (PUT, GET, CPY, LOAD or
STORE) or a wait. Similarly, any wait can be delayed before a
read/write on a shared variable or a notify. We also take into account
the control flow of the program and rules to conform to the UPC
language specification, which for example prohibits nested barriers.  For instance, it is not allowed to have the following sequence: \texttt{notify} \texttt{notify} \texttt{wait} \texttt{wait} in a UPC application.


We also provide an analysis to move communication completion across
barriers while ensuring there are no data races. This occurs when the code
contains redundant barriers or it is over-synchronized. In this case to
account for control divergence, for each {\tt init}  call we complete
it after the minimum number of barriers observed across all its
occurrences in  all traces. This is a conservative heuristic that
simplifies the need for complex control flow information. 


Custom synchronization patterns are optimized in a similar manner, our
analysis for semaphores omitted for brevity. The gist of the
optimization is that we associate all communication and  synchronization
operations with the same target and then delay completion of
communication across all other independent synchronization, of course
subject to  data dependencies.


