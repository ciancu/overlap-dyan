\section{Experimental Results}
\label{sec:results}

We evaluate our optimizations on two supercomputers with different functionality and
performance characteristics: Edison and Shepard.
Edison (Cray-XC30) is deployed at NERSC~\cite{edison}.
Its nodes contain two 12-core Ivy Bridge
processors running at 2.4~GHz and are connected using the Aries~\cite{aries} network.
Every four nodes are connected to one Aries network interface chip.
The Aries chips form a 3-rank dragonfly network.

Shepard~\cite{shepard} is deployed at Sandia National Laboratory~\cite{shepard} and its
nodes have two 16-core Haswell processors at 2.3GHz.
The interconnect is Mellanox InfiniBand FDR.

\comment{Aries showcases a network with aggressive out-of-order delivery support and
hardware acceleration: FMA for small messages, BTE for large
messages.  In contrast, InfiniBand still
employs source  routing with strict ordering in reliable mode and the CPU is
more heavily involved in message injection.
}
The instrumentation of memory accesses, one-sided operations and synchronizations 
are done with a compiler pass.
We use LLVM 3.7.1~\cite{LLVMwebsite} and Berkeley Unified
Parallel~C, v.~2.22.0~\cite{BUPCwebsite}.
The pass inserts function calls into our lightweight runtime.
The trace file analysis is implemented in C++.
Experimental results are averaged from five runs.


\subsection{Benchmarks Description}

We select applications with different communication characteristics to showcase the
impact of our approach. 
miniGMG~\cite{SC12_CAMG}\cite{MGwebsite} is a grid based code that
performs nearest (six or 26) neighbor communication on a 3-D processor
grid. 
HipMer~\cite{Georganas2015}\cite{Meracoulouswebsite} implements a De Novo Genome
Assembler and exhibits highly irregular message patterns that vary
both the  number and the destination of messages in each communication
step.
We also evaluate our approach on  the UPC NAS~\cite{npb} Parallel
Benchmarks~v2.3, using the  BT, SP, IS, FT and LU applications. 
For miniGMG and the NAS parallel versions we have  implementations
using  blocking communication written in stock UPC, as well as hand optimized
versions with communication overlap using non-blocking
extensions. HipMer uses  only blocking communication. All benchmarks
were developed and optimized by third parties and obtained from public domain.
miniGMG is written in UPC+OpenMP and can be
configured as SPMD or hybrid parallelism. HipMer is written in UPC and
runs only in SPMD mode.  For brevity we do not describe the NAS
Parallel Benchmarks, for details please consult ~\cite{npb}.

\input{miniGMG}
\input{hipmer}


\subsection{Performance Evaluation}



\subsubsection{Maximal Overlap Results}


In this section, \emph{BLOCKING} denotes the  version of a benchmark
that uses blocking communication, \emph{NONBLOCKING} denotes the hand
optimized version, if any available,  and \emph{OPTIMIZED} denotes the
version optimized by our tool. 

When starting with a blocking implementation the tool always found
potential for optimization across all benchmarks. The improvements in
end-to-end application 
performance are good and  match or exceed the performance of
hand optimized codes. The optimizations always reduce significantly
the time spent in communication by the application. These trends are
illustrated in
Figures~\ref{fig:BvsNBvsOPT_NASBT_total},~\ref{fig:BvsNBvsOPT_NASBT} and~\ref{fig:BvsNB_hipmer}. Note that for HipMer we have available only the blocking
implementation and we were able to  reduce  the time by 40\% with 1,040 threads.


%miniGMG is faster by up to 63\% with our optimizer compared to the blocking version

When starting with a hand optimized implementation the tool found new
optimizations for BT, SP, LU and some versions of miniGMG. All these
applications were optimized using a similar strategy, which
incidentally occurs in most SPMD optimized codes. The code is written such
that each task performs in one
logical step domain boundary exchanges with all its neighbors, followed by
synchronization with all neighbors. Any operations for a single
boundary are overlapped for latency hiding, but due to code complexity
exchanges for multiple  boundaries are not. The tool was able to overlap all
communication across all boundaries and reduce  both time spent in communication
and end-to-end execution time. For example, when looking at BT
communication time in Figure~\ref{fig:BvsNBvsOPT_NASBT}, our optimized version takes less time
from 576 threads and is faster by 26\% with 784 threads. When
  looking at end-to-end execution in
  Figure~\ref{fig:BvsNBvsOPT_NASBT_total} we observe 7\% when using
  900 cores. 
  


When examining the results, one trend becomes notable. At high
concurrency our optimizations always help. At low concurrency,
providing maximal overlap sometimes hurts performance. With strong
scaling, messages become  large at low concurrency. As indicated
by Luo et al~\cite{Luo:2012} on the networks we use (Aries and
InfiniBand) issuing simultaneously a large number of large messages
degrades performance.

% NAS 




\begin{figure}[h!]
\begin{center}
\includegraphics[width=8cm,height=1.5in]{DATA/NAS/HISTO_EXECUTION_TIME_BT_D}
\caption{Execution-time for blocking, nonblocking and optimized versions of NAS BT, Class D (Strong scaling). Results obtained on Edison.}
\label{fig:BvsNBvsOPT_NASBT_total}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=8cm,height=1.5in]{DATA/NAS/HISTO_EXECUTION_TIME_BT_COMM_D}
\caption{Time spent in communication for NAS BT, Class D (Strong scaling). Results obtained on Edison.}
\label{fig:BvsNBvsOPT_NASBT}
\end{center}
\end{figure}


% HipMer



\begin{figure}[h!]
\begin{center}
\includegraphics[width=8cm,height=1.5in]{DATA/Hipmer/EXECUTION_TIME_meracoulous_BvsNBvsOPT_Timer}
\caption{Time spent in communication for blocking and optimized versions of HipMer. Results obtained on Edison.}
\label{fig:BvsNB_hipmer}
\end{center}
\end{figure}




\subsubsection{Optimizing Across Barriers and Bespoke Synchronization}


As all benchmarks use barrier synchronization, our tool was able to
find places to exploit the split-phase barrier available in
GASNet. This is an example of cross module optimization spanning
levels of abstraction that is beyond the reach of most, if not all,  application
developers.  In BT, SP, FT, IS and LU the transformations did not lead
to performance improvements, as the analysis found this potential in
the initialization and tear-down code, and not in the time intensive
computations. In all
benchmarks we found multiple opportunities to overlap {\tt malloc} with
barriers, overlap  {\tt printf}
with barrier, etc.

 Figure \ref{fig:IMM_miniGMG_BAR} shows the
miniGMG 
speedup  when delaying communication completion  into  the
implementation of the  barrier call. Except for 16
threads, using our tool improves the performance of this already
optimized code. We obtained up to 64\% performance improvements in
communication time when compared to the nonblocking version (with 512 threads). Again, the
performance improvements get more pronounced at high concurrency. 

 

Besides transforming blocking communication into overlapped
non-blocking communication, our tool detected redundant barriers in
HipMer. It also advised  to delay  communication completion  before the last redundant barrier.
Again, this would be a very complex transformation to perform manually.
%%%%%%%%%%%%% Nonblocking barriers



\begin{figure}[h!]
\begin{center}
\includegraphics[width=9cm,height=1.5in]{DATA/MG/IMM/HISTO_SPEEDUP_miniGMG_5444_IMM_BAR}
\caption{Speedup in communication when using nonblocking communication and our optimized version that delays synchronizations into nonblocking barriers of miniGMG, IMM version, (weak scaling). Results obtained on Edison.}
\label{fig:IMM_miniGMG_BAR}
\end{center}
\end{figure}



%%%%%%%%%%%%% Semaphores

When considering optimizations across bespoke synchronization
patterns, again our optimizer is able to improve performance. Figures
 \ref{fig:AGG_miniGMG}, \ref{fig:AGG_miniGMG_S} and \ref{fig:IMM_miniGMG} present the results
for the AGG and IMM versions respectively. Figures  \ref{fig:AGG_miniGMG} and \ref{fig:IMM_miniGMG} show the results obtained on Edison whereas Figure \ref{fig:AGG_miniGMG_S} shows the results obtained on Shepard.  The gray bars represent
the speedup we gain in addition to the version of the algorithm that
implements  a Producer-Consumer pattern. Results are omitted for
brevity, but this is the fastest  implementation of this
application. After applying our tool we observe performance
improvements as high as 29\% (with 1,024 threads for the IMM version on Edison). These improvements come from the
ability to mix communication completion with independent ad-hoc
(semaphores)
synchronization operations.  Note that Shepard does not have enough nodes to get the benefit we have on Edison.



% miniGMG AGGREGATE 

\begin{figure}[h!]
\begin{center}
\includegraphics[width=9cm,height=1.5in]{DATA/MG/AGG/HISTO_SPEEDUP_miniGMG_5444}
\caption{Speedup in communication when using nonblocking communication and our optimized version that delays synchronizations across semaphores of miniGMG, AGG version PCDB  (weak scaling) Results obtained on Edison.}
\label{fig:AGG_miniGMG}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=9cm,height=1.5in]{DATA/MG/AGG/HISTO_SPEEDUP_miniGMG_5444_SHEPPARD}
\caption{Speedup in communication when using nonblocking communication and our optimized version of miniGMG, AGG version PC (weak scaling). Results obtained on Shepard.}
\label{fig:AGG_miniGMG_S}
\end{center}
\end{figure}

%miniGMG IMMEDIATE

\begin{figure}[h!]
\begin{center}
\includegraphics[width=9cm,height=1.5in]{DATA/MG/IMM/HISTO_SPEEDUP_miniGMG_5444_IMM}
\caption{Speedup in communication when using nonblocking communication and our optimized version of miniGMG, IMM version PCDB (weak scaling). Results obtained on Edison.}
\label{fig:IMM_miniGMG}
\end{center}
\end{figure}

\subsubsection{Analysis Overhead} Table \ref{tab:EXECtime}  shows the
slowdown to generate a trace file, when running at small scale and on small inputs. 
All NAS benchmarks except LU (4) have been launched with 16 processes and
Class  S (small). 
miniGMG was launched with 32 threads while HipMer was launched with 48
threads. Currently the tracing overhead is high, but we know how to  
reduce it given engineering resources. As explained, the good news is
that for the programs considered a single run  is enough to infer
most of the information needed for optimizations. We believe this
happens for most SPMD or hybrid parallelism SPMD+X codes.  

\begin{table}[ht!]
\tiny
\centering
\caption{Execution-time overhead induced by the tool to generate a trace file.}
{\small
\begin{tabular}{|c|c|c|c|} \hline 
 \multirow{2}*{{\bf{Benchmark}}} &  \multicolumn{2}{c|}{{\bf{Execution Time (in sec)}}}  		& {\bf{Averaged}}				\\ \cline{2-3}
				      		& {\bf{without the tool}} 	  	&	{\bf{with the tool}}    		& 	{\bf{ratio}}					\\ \hline
%\small{miniGMG AGG}	     	& 						& 						&							\\ \hline
\small{miniGMG IMM}	     	& 	12.517				& 	865.905				&	69						\\ \hline
\small{NAS BT}	     			&	0.09					& 	10,327.705			&	114,752						\\ \hline
\small{NAS SP}	     			&	0.086				& 	1,792.154				&	20,839						\\ \hline
\small{NAS IS} 	     			&  	0.01					& 	8.63					&	863						\\ \hline
\small{NAS FT}	   			&  	0.063				& 	244.83				&	3,886						 \\ \hline
\small{NAS LU}	   			&  	0.058				&     1,195.0525 			&      20,604							 \\ \hline
\small{HipMer}	   			&  	15.934					& 	225.449					&	14						\\ \hline
\end{tabular}
}
\label{tab:EXECtime}
\end{table}

% miniGMG: results for nonblocking version, 32 threads
% NAS
% HipMer : results for blocking version, 48 threads 


\comment{
The trace files give all information needed to perform the optimizations we propose. 
The following subsections report the performance results we were able
to obtain.

\begin{figure}[h!]
\centering
\begin{lstlisting}
PUT line 162 in copy_faces.c must be completed before LOAD line 380 in copy_faces.c -> OVERLAP:1994 usec 
PUT line 284 in copy_faces.c must be completed before LOAD line 380 in copy_faces.c -> OVERLAP:1340 usec 
PUT line 103 in copy_faces.c must be completed before BARRIER line 401 in copy_faces.c -> OVERLAP:6789 usec 
PUT line 223 in copy_faces.c must be completed before BARRIER line 401 in copy_faces.c -> OVERLAP:6226 usec 
PUT line 367 in copy_faces.c must be completed before BARRIER line 401 in copy_faces.c -> OVERLAP:4530 usec 
PUT line 399 in copy_faces.c must be completed before BARRIER line 401 in copy_faces.c -> OVERLAP:36 usec 
\end{lstlisting}
\caption{\label{fig:trace_analysis_ex} Trace Analysis example.}
\end{figure}}

