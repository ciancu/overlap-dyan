\section{Transformations to Maximize Overlap}
\label{sec:trans} 

We will use the  simplified 2-D stencil operation example in Figure~\ref{fig:overlap} to illustrate the target transformations.  In each computation step boundary data is exchanged with {\tt pack\_send\_boundary}, then tasks synchronize ({\tt 5:})and proceed to update the local data domain at {\tt 6:}. In the unoptimized versions of the code communication initiated in  {\tt send\_domain} is blocking and completed in place. This was the case in our HipMer benchmark. Most common optimization is to overlap the transmission with the 
boundary processing and have all communication be completed at {\tt 3:} before returning from {\tt pack\_send\_boundary}.  This was the case in most of our benchmarks optimized by third party developers: NAS BT, SP, IS, FT and versions of miniGMG.  More ambitious optimizations may delay and complete all communication initiated in any {\tt send\_subdomain} call at the program point {\tt 4:}.  As this  requires tracking per operation state across procedure or library boundaries, this optimization is seldom encountered in codes.  For our study this was the case in the miniGMG application, which we have  most thoroughly optimized for performance. 


\begin{figure}[h!]
\centering
\begin{lstlisting}
  pack_send_boundary(neighbor) {
    foreach subdomain(neighbor)
1:      pack_subdomain() //local work
2:      send_subdomain() // init, wait pair
3: // can wait on all puts for this neighbor
}
... 
{   foreach neighbor (E, W, N, S)
        pack_send_boundary(neighbor)
4: // can wait for all puts for all neighbors
5:  sync_with_neighbors(E, W, N, S) //semaphore, barrier ..
6:  update_domain()}
\end{lstlisting}
\vspace{-.1in}
\caption{\label{fig:overlap} Overlap example.}
\vspace{-.2in}
\end{figure}



From this discussion, it becomes apparent that most codes are likely to provide room for more overlap optimizations: the ideal behavior has
the completion for any communication operation delayed until the data involved in the operation is touched locally. For example, for a {\it Put} operation, completion can be delayed until the first store instruction in the program execution that overwrites any bytes involved in the payload. The goal of our first optimization is to transform the codes for maximal overlap and delay communication completion based on the dynamic data dependencies. For this we need to be able to reason about the data accesses during runtime, combined with a framework to automatically track outstanding communication operations and associate their completion with the correct program scope or point in the control flow graph.

\subsection{Complex Overlap Transformations}

Although cumbersome, there are situations where developers may be able to attain maximal overlap with respect to local dependencies.  The next class of transformations we target are most likely outside the reach of manual optimization, even in relatively small codes. Going back to the example in Figure~\ref{fig:overlap}, the insight is that completion of the operations initiated in {\tt send\_domain} may be delayed either until 
within {\tt sync\_with\_neighbors}, or across it and  until within {\tt update\_domain}. 
Depending on the algorithm, {\tt sync\_with\_neighbors} can be a proper barrier operation. In this case the optimization will require delaying completion of application level communication well within the implementation of a runtime library function. Alternately, it can also implement ad-hoc synchronization algorithms. We develop transformations to handle both scenarios. 


Consider the case where threads synchronize using barrier operations. GASNet provides support for split phase\footnote{This permeates up to the UPC language level so our approach has been trivially extended to handle codes with explicit split-phase barriers.} barriers. 
A split-phase barrier is   composed of two phases: \emph{barrier\_notify} and
\emph{barrier\_wait}. In the notification phase, a thread notifies others that
it is ready for the barrier. In the wait phase, a thread waits for
others to be ready. A UPC  {\tt upc\_barrier} operation is implemented as a sequence of barrier notify and wait calls.  
Our optimizer always treats a {\tt upc\_barrier} as its constituents and attempts several transformations:
\begin{itemize}
 \item Move notify and wait operations as far apart in the code as possible, subject to data dependencies. This overlaps barrier latency with other work. This illustrates a scenario where execution of library modules is split and mixed with application execution. 
\item Move completion of communication operations that precedes barriers in between the notify/wait calls. This enhances the communication overlap and illustrates a transformation where  application level code is moved inside runtime libraries.
\item Move completion of communication operations across multiple barriers, subject to data dependencies. 
\end{itemize}

First two transformations are examples of optimizations that address functional semantic mismatches between layers of the software stack, while the last  addresses logical mismatches.

For codes using one-sided communication, one common~\cite{Shan:2015,Husbands:2007} transformation for performance is replacing collective or group based synchronization with point-to-point communication and synchronization operations. In this case, the transformed code ends up implementing in most cases a producer-consumer pattern using ad-hoc synchronization primitives.  This can be abstracted as follows. Note that on the left hand side we present the base case, while on the right hand side we present the pattern that actually occurs in optimized applications, where all communication and synchronization operations are grouped together. For example, this pattern which we implemented for miniGMG occurs also   in Particle-In-Cell codes, as well as the code described in~\cite{Shan:2015}.

%\begin{figure}[h!]
%\centering
\begin{lstlisting}
wait_one(empty)                            wait_all(empty)
//produce               OR                  //produce
put ..                                           put ..
put..                                            put..
..                                                   ..
signal_one(full)                           signal_all(full)
\end{lstlisting}
%\end{figure}


The transformation we desire to perform in this case is moving the completion of communication operations across these ad-hoc synchronization primitives, subject to data dependencies. In our study we selected codes using the Berkeley UPC semaphore library extension. In order to support these transformations, the tool-chain needs to provide support for  marking  and reasoning  about ``arbitrary'' portions of code.